public class FindGrade{
    public static void main(String[] args) {
        int score = Integer.parseInt(args[0]);

        if (score > 100){
            System.out.println("its not a valid score");
        }else if(90 <= score & score <= 100) {
            System.out.println("your grade is A");
        }else if(80 <= score & score < 90) {
            System.out.println("your grade is B");
        }else if(70 <= score & score < 80){
            System.out.println("your grade is C");
        }else if(60 <= score & score < 70) {
            System.out.println("your grade is D");
        }else if(0 <= score & score < 60){
            System.out.println("your grade is F");
        }else{
            System.out.println("its not a valid score");
        }
    }
}